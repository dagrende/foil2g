class Point:
    """ Point class for representing and manipulating x,y coordinates. """

    def __init__(self, x=0, y=0):
        """ Create a new point at x, y """
        self.x = x
        self.y = y

    # returns this point scaled by scale as a new Point
    def scaled(self, scale):
        return Point(self.x * scale, self.y * scale)
    
    def getTuple(self):
        return (self.x, self.y)

    def __repr__(self):
        return f"Point({self.x}, {self.y})"
