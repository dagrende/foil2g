import sys
from airfoil import AirFoil
from point import Point
import argparse
import pyclipper

parser = argparse.ArgumentParser(description='generate g-code for hot wire wing profile cutting')

parser.add_argument('foil_path', type=str,
                    help='path to foil dat file')
parser.add_argument('--width', type=float, default=100.0,
                    help='wing width (front to back) [mm]')
parser.add_argument('--vsafe', type=float, default=5.0,
                    help='safe vertical distance above top [mm]')
parser.add_argument('--vmarg', type=float, default=3.0,
                    help='top of wing level below 0 [mm]')
parser.add_argument('--hmarg', type=float, default=5.0,
                    help='horisontal move before back edge [mm]')
parser.add_argument('--cut_speed', type=float, default=300.0,
                    help='cut speed [mm/min]')
parser.add_argument('--radius', type=float, default=1.5,
                    help='tool radius [mm]')

cmdLine = "python " + " ".join(sys.argv)

args = parser.parse_args()

# returns points compensated for tool radius r, as a new Point list
# points must be a series of points going clockwise around the airfoil
def radiusCompensate(points, r):
    if r < 0.01:
        return points
    tuples = list(map(lambda p: p.getTuple(), points))
    pco = pyclipper.PyclipperOffset()
    pco.AddPath(pyclipper.scale_to_clipper(tuples), pyclipper.JT_MITER, pyclipper.ET_CLOSEDPOLYGON)
    compensated = pco.Execute(pyclipper.scale_to_clipper(args.radius / args.width))
    compensatedPoints = [Point(lp[0], lp[1]) for lp in pyclipper.scale_from_clipper(compensated[0])]
    # shift points so that leftmost points starts and ends the polygon
    # add index to points
    indexPoints = [{'i': i, 'p': p} for i,p in enumerate(compensatedPoints)]
    # sort by x
    sortedIndexPoints = sorted(indexPoints, key=lambda ip: -ip['p'].x)
    # get two leftmost points sorted by y
    leftmostTwoIndexPoints = sorted(sortedIndexPoints[0:2], key=lambda ip: -ip['p'].y)
    # shift points to get polygon starting in top left point
    startPointIndex = leftmostTwoIndexPoints[0]['i']
    result = compensatedPoints[startPointIndex: len(compensatedPoints)] + compensatedPoints[0:startPointIndex]
    return result

foilPath = sys.argv[1]
foil = AirFoil(args.foil_path)
min = foil.coordRange['min']
max = foil.coordRange['max']
print(f"min=({'%.1f' % (min.x * args.width)}, {'%.1f' % (min.y * args.width)}) " +
    f"max=({'%.1f' % (max.x * args.width)}, {'%.1f' % (max.y * args.width)}) " +
    f"size=({'%.1f' % ((max.x - min.x) * args.width)}, "+
        f"{'%.1f' % ((max.y - min.y) * args.width)}) ", file=sys.stderr)

compensatedPoints = radiusCompensate(foil.points, args.radius)

# make back to front edge go from 0 to args.width, make y go from 0 and down
# FIXME find back edge
# assume start x is back edge
# assume back x is > front x
# assume 0 <= x <= 1
points = []
for p in compensatedPoints:
    points.append(Point(
        -(p.x - max.x) * args.width + args.hmarg,
        (p.y - max.y) * args.width - args.vmarg
    ))

def coord(p):
    return '{0:.5f}'.format(p)

def coordsVert(p):
    return f"b{coord(p.y)} y{coord(p.y)}"

def coordsHor(p):
    return f"a{coord(p.x)} x{coord(p.x)}"

def coords(p):
    return f"{coordsHor(p)} {coordsVert(p)}"

print("(" + cmdLine + ")")
safePos = Point(0, args.vsafe)
print(f"g0 {coordsVert(safePos)}")
print(f"g0 {coordsHor(safePos)}")
print(f"g1 f{args.cut_speed}")
print(f"g1 {coordsVert(points[0])}")
for p in points:
    print(coords(p))
print(f"g1 {coordsHor(safePos)}")
print(f"g1 {coordsVert(safePos)}")

